export const ADD_TO_CART = "ADD_TO_CART";
export const DELETE_ITEM = "DELETE_ITEM";

export const addCart = (payload) => ({
	type: ADD_TO_CART,
	payload
})

export const deleteItem = (payload) => ({
    type : DELETE_ITEM,
    payload
});