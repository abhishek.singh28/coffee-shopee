import {Routes, Route } from "react-router-dom"
import Cart from "./Components/Cart"
import { Home } from "./Components/Home"
import Navbar from "./Components/Navbar"



function App() {

  return (
    <div className="App" style={{backgroundColor: "papayawhip"}}>
      <Navbar />
      <Routes>
        <Route path='/' element={ <Home /> }></Route>
        <Route path='/cart' element={ <Cart /> }></Route>
      </Routes>
    </div>
  )
  
}

export default App
