import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { styled } from '@mui/material/styles';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import MenuItem from "@mui/material/MenuItem";
import FavoriteIcon from '@mui/icons-material/Favorite';
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import {useState,useEffect} from 'react'
import axios from 'axios';
import { Home } from './Home';
import { connect } from "react-redux";
import { addCart } from '../Redux/action';




const StyledToolbar = styled(Toolbar)({
    display: "flex",
    justifyContent:"space-evenly"

}) 
const StyledTypography = styled(Typography)({
    display: "flex",
    justifyContent:"space-around",
    ":hover":{
        cursor:"pointer"
    }
})


// const pages = ["Products", ];

const Navbar = ({cart}) =>{

    const car = useSelector(store =>store.cart);
    console.log("Cart",car.length)
    // const quantity = cart.length;
    const dispatch = useDispatch()

    const [cartLength, setCartLength] =useState([])
    const [quantity, setQuantity] =useState(0) 



    useEffect(() => {
		getData()
    
	},[ quantity])
  console.log("Dataa", cartLength)

  // const getData = () => {
  //   axios.get("http://localhost:5000/cart")
	// 	.then((res) => {
	// 		setCartLength(res.data)
  //     console.log("Res", res)
      
  //           setQuantity(cartLength.length)
	// })
    
  // }

  const getData = async () => {
    try{
    const res = await fetch("http://localhost:5000/cart");
    const json = await res.json()
    // setCartLength(json);
    // console.log("JSON", json.length)
    // setQuantity(cartLength.length)
    dispatch(addCart(json))
    }
    catch(err){
      console.log("Error found", err);
    }
  }
  

    return(
        <AppBar position='sticky' style={{backgroundColor: "black" , color:"#EFD345"}}>
            
        <StyledToolbar>
             
         <StyledTypography sx={{display:"flex", justifyContent:"space-evenly", gap:"30px"}}>
		 <img src='https://c8.alamy.com/compit/w4n9h1/il-logo-del-vettore-per-coffee-house-w4n9h1.jpg' style={{height: "80px"}} />
         <h2><Link to={"/"} style={{textDecoration:"none", color:"#EFD345"}} >COFFEE HOUSE</Link></h2>
		 {/* {pages.map((page) => (
                <MenuItem key={page} >
                  <Link to={"/product"} style={{textDecoration:"none", color:"#EFD345"}}><StyledTypography textAlign="center">{page}</StyledTypography></Link>
                </MenuItem>
              ))} */}
           
         </StyledTypography>
         <div>
            <input type="text" placeholder="Enter Search here" style={{width:"300px", height:"30px", borderRadius: "10px"}} />
            <button style={{width:"100px", height:"25px", borderRadius: "7px", fontSize:"10px", marginLeft:"5px"}}>Search</button>
         </div>

         <h3><Link to={"/orders"} style={{textDecoration: "none", color: "#EFD345"}}>My Orders</Link></h3>
         
        
         <StyledTypography sx={{display:"flex",justifyContent:"space-between",gap:"30px"}}>
         
             <AccountCircleIcon ></AccountCircleIcon>
             <FavoriteIcon></FavoriteIcon>
             <Link to={"/cart"} style={{textDecoration:"none", color:"#EFD345"}}><ShoppingCartIcon></ShoppingCartIcon></Link>
             <span>{quantity}</span>
         </StyledTypography>
        </StyledToolbar>
        </AppBar>
    )
}

const mapStateToProps = (state) => {
  return {
    cart: state.cart,
  };
};

export default connect(mapStateToProps)(Navbar);