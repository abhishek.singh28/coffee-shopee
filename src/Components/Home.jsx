import axios from "axios";
import { useEffect, useState } from "react";
// import './App.css'
import styled from "styled-components";
// import {Navbar} from './Navbar'
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { addCart } from "../Redux/action";


export const Home = () => {

    const [data, setData] = useState([]);
  const [view, setView] = useState(true);
  // const [cart, setCart] = useState([]);
  const [sortedData, setSortedData] = useState({parameter: "", value:""})
  const dispatch = useDispatch()

  // const cart= useSelector((store) => {
	// 	return store.cart
	// })
  


  const fetchData = () => {
    
    axios("http://localhost:5000/coffee", {
      method: "GET"
    })
      .then((res) => {
        setData(res.data);
      })
      .catch((err) => {
        console.log("Error", err);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleChange = () => {
    if(view == true)
    {
      setView(false)
    }
    else{
      setView(true)
    }
  }

  const Grid = styled.div`
    display: grid;
    grid-template-columns : repeat(3,1fr);
    gap: 50px;
    width: 95%;
    margin: auto;
    color: black;
    margin-top: 50px;
  `;

  const addToCart = (el) => {
    console.log("xxxxx")
    axios.post("http://localhost:5000/cart",{
			title:el.title,
			price:el.price,
			src: el.src,
			rating: el.rating,
			id: el.id
		})
    .then((res)=> {
      console.log("Resss",res.data)
      dispatch(addCart(res.data))
    })
  };

  const handleSort = (parameter, value) => {
		setSortedData({parameter, value})
	}

  return (
    <div style={{backgroundColor: "gray"}}>

      {/* <Navbar /> */}

		<h1 style={{display: "flex", alignItems: "center", justifyContent: "center"}}>My Coffee House</h1>

    <div style={{display: "flex", alignItems: "center", justifyContent: "center"}}>
      View: <button style={{marginRight:"50px"}} onClick={handleChange}>Change view</button>
          

      Sort by:
        <button onClick={()=>handleSort("price", -1)}>High to Low</button>
        <button onClick={()=>handleSort("price",1)}>Low to High</button> 

        </div>    


        {view ? 

      <Grid>
        {data
        .sort((a,b) => {
          if(sortedData.parameter=="price" && sortedData.value== 1)
          {
            return a["price"] - b["price"]
          }
    
          else if(sortedData.parameter=="price" && sortedData.value== -1)
          {
            return b.price - a.price
          }
        })
        .map((el) => (
          
          <div key={el.id} style={{ height: "400px", backgroundColor:"#cd7e45",borderRadius:"10px", boxShadow: "rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px, rgba(10, 37, 64, 0.35) 0px -2px 6px 0px inset", color: "black" }}>
            
            <img src={el.src} alt="" style={{width: "90%", height: "70%", marginLeft:"15px", borderRadius:"10px", marginTop: "15px" }} />

            
            <div style={{width: "90%", margin:"auto"}}>
              <h3 style={{marginTop: "10px"}}>{el.title}</h3>
              <div style={{display: "flex", justifyContent:"space-between"}}>
                <p style={{marginTop: "-10px", fontWeight: "bold"}}>Price: $ {el.price}</p>
                <AddShoppingCartIcon onClick= {() => addToCart(el)}></AddShoppingCartIcon>
              </div>
           </div>

          </div>
        ))}
        </Grid>
        
        :

        data.map((item) => (
        //   <Link to={`/coffee/${item.id}`} >
            <div style={{ height: "400px",width: "400px", margin:"auto", backgroundColor:"#cd7e45", marginTop:"30px", borderRadius: "10px", color: "black" }} key={item.id}>

          <img src={item.src} style={{width: "90%", height: "70%", marginLeft:"15px", borderRadius:"10px", marginTop: "15px"}} />
          <div style={{width: "90%", margin:"auto"}}>
              <h3 style={{marginTop: "10px"}}>{item.title}</h3>
              <div style={{display: "flex", justifyContent:"space-between"}}>
                <p style={{marginTop: "-10px", fontWeight: "bold"}}>Price: $ {item.price}</p>
                <AddShoppingCartIcon onClick= {() => addToCart(item)}></AddShoppingCartIcon>
              </div>
           </div>
           </div>
        // </Link>
        ))
        
    }
    
    </div>

  );
}