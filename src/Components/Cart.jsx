import React,{useState,useEffect} from 'react'
import './Cart.css';
import LocalShippingRoundedIcon from '@mui/icons-material/LocalShippingRounded';
import { Link } from 'react-router-dom';
import axios from 'axios';
import styled from "styled-components"
import { useDispatch, useSelector } from "react-redux"
import { addCart } from "../Redux/action";
import { deleteItem } from '../Redux/action';
// import { Navbar } from './Navbar';


const Cart = () => {

	const [cartData, setCartData] =useState([])
	const [product, setProduct] = useState(0)

	const dispatch= useDispatch();

	const cart= useSelector((store) => {
		return store.cart
	})


  let sum;

	useEffect(() => {
		getData()
	},[cartData.length])

  const getData = () => {
    axios.get("http://localhost:5000/cart")
		.then((res) => {
			setCartData(res.data)
			dispatch(addCart(cartData))
	})
  sum = 0;
			for(let i=0;i<cartData.length;i++){
			sum += Number(cartData[i].price)
		}
    setProduct(sum)
  }

  const handleDelete=(id) => {
		console.log("Delete")
		axios.delete(`http://localhost:5000/cart/${id}`).then(() => {
			dispatch(deleteItem(id))
			getData();
			
		})
	}

	

	const Container = styled.div`
	width: 100%;
	margin: auto;
	display: grid;
	grid-template-columns: repeat(4,1fr);
	gap: 30px;
	margin-top: 30px
	`

	const Card = styled.div`
	height: 300px;
	background-color: #cd7e45;
	margin-left: 20px;
	border-radius: 10px;
	box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px, rgba(10, 37, 64, 0.35) 0px -2px 6px 0px inset;
	padding: 10px
	`
    
  return (
    <>
    {/* <Navbar /> */}
      <div style={{color: "black", width: "90%", margin: "auto"}}>
        <div>
          <h1>My Cart</h1>

          <div id="line"></div>
		  <p id="total_price" >
            Subtotal({cartData.length} items):{" "}
            <span className="text-xl font-bold">$ {product}</span>
          </p>

		  <Container>
          {cart.map((el) => {
			return <Card key={el.id}>
				<img src={el.src} alt="Something wrong" style={{height: "45%", width:"75%",marginTop: "10px", borderRadius: "10px"}} />
				<h5>{el.title}</h5>
				<p>Price: {el.price}</p>
				<button style={{height: "20px", width: "120px", fontSize:"11px"}} onClick={() => {handleDelete(el.id)}}>
          Remove from Cart</button>
				</Card>
				
			})
		}
		</Container>
         
        </div>

        <div style={{width: "90%", margin : "auto"}}>
          <p>ORDER SUMMARY</p>
          <div id="line"></div>
          <div>
            <div>Total for Items</div>
            <div>$ {product}</div>
          </div>
          <div id="line"></div>
          
          <div>
            <LocalShippingRoundedIcon />
            <p>
             FREE shipping.
            </p>
          </div>
          <Link to="/checkout"><button>
            CHECKOUT
          </button></Link>
          <p>
            Taxes and shipping will be calculated in the checkout window.
          </p>
        </div>
      </div>
      
    </>
  );
}

export default Cart